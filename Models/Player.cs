using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace pga_tour.Models
{
    public class Player
    {
      [Key]
      public int Id { get; set; }
      public string FirstName { get; set; }
      public string LastName { get; set; }
      public string Nationality { get; set; }
      public float Height { get; set; }  
      public float Weight { get; set; }
      public DateTime Birthday { get; set; }
      public DateTime? TurnedPro { get; set; }
      public Boolean IsAmateur { get; set; }
      public byte[] ProfilePicture { get; set; }

      [InverseProperty("Player")]
      public IEnumerable<Scorecard> Scorecards { get; set; } 

      public Player() { }

      public Player(string firstName, string lastName, string nationality,
       float height, float weight, DateTime birthday, DateTime turnedPro, 
       Boolean isAmateur, byte[] profilePicture)
      {
        FirstName = firstName;
        LastName = lastName;
        Nationality = nationality;
        Height = height;
        Weight = weight;
        Birthday = birthday;
        TurnedPro = turnedPro;
        IsAmateur = isAmateur;
        ProfilePicture = profilePicture;
      }

      public Player EditPlayer(Player player)
      {
          FirstName = player.FirstName;
          LastName = player.LastName;
          Nationality = player.Nationality;
          Height = player.Height;
          Weight = player.Weight;
          Birthday = player.Birthday;
          TurnedPro = player.TurnedPro;
          IsAmateur = player.IsAmateur;
          ProfilePicture = player.ProfilePicture;
          return this;
      }

    }
}
