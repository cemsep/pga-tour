using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace pga_tour.Models
{
    public class Course
    {
      [Key]
      public int Id { get; set; }
      public string CourseName { get; set; }
      public string Location { get; set; }
      public int Par { get; set; }
      public int Length { get; set; }
      public IEnumerable<Hole> Hole { get; set; }

      public Course() { }

      public Course(string courseName, string location, int par, int length)
      {
        CourseName = courseName;
        Location = location;
        Par = par;
        Length = length;
      }

    }
}
