using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace pga_tour.Models
{
    public class Scorecard
    {
        [Key]
        public int Id { get; set; }
        public int Hole1 { get; set; }
        public int Hole2 { get; set; }
        public int Hole3 { get; set; }
        public int Hole4 { get; set; }
        public int Hole5 { get; set; }
        public int Hole6 { get; set; }
        public int Hole7 { get; set; }
        public int Hole8 { get; set; }
        public int Hole9 { get; set; }
        public int Hole10 { get; set; }
        public int Hole11 { get; set; }
        public int Hole12 { get; set; }
        public int Hole13 { get; set; }
        public int Hole14 { get; set; }
        public int Hole15 { get; set; }
        public int Hole16 { get; set; }
        public int Hole17 { get; set; }
        public int Hole18 { get; set; }
        public int Out { get; set; }
        public int In { get; set; }
        public int Total { get; set; }

        [ForeignKey("Player")]
        public int PlayerId { get; set; }
        public Player Player { get; set; }

        public Scorecard() { }

        public Scorecard(int hole1, int hole2, int hole3, int hole4, 
            int hole5, int hole6, int hole7, int hole8, int hole9,
            int hole10, int hole11, int hole12, int hole13, int hole14,
            int hole15, int hole16, int hole17, int hole18, Player player)
        {
            Hole1 = hole1;
            Hole2 = hole2;
            Hole3 = hole3;
            Hole4 = hole4;
            Hole5 = hole5;
            Hole6 = hole6;
            Hole7 = hole7;
            Hole8 = hole8;
            Hole9 = hole9;
            Hole10 = hole10;
            Hole11 = hole11;
            Hole12 = hole12;
            Hole13 = hole13;
            Hole14 = hole14;
            Hole15 = hole15;
            Hole16 = hole16;
            Hole17 = hole17;
            Hole18 = hole18;
            Out = Hole1 + Hole2 + Hole3 + Hole4 + Hole5 + Hole6 + Hole7 + Hole8 + Hole9;
            In = Hole10 + Hole11 + Hole12 + Hole13 + Hole14 + Hole15 + Hole16 + Hole17 + Hole18;
            Total = Out + In;
            Player = player;
        }

        public Scorecard EditScorecard(Scorecard scorecard)
        {
            Hole1 = scorecard.Hole1;
            Hole2 = scorecard.Hole2;
            Hole3 = scorecard.Hole3;
            Hole4 = scorecard.Hole4;
            Hole5 = scorecard.Hole5;
            Hole6 = scorecard.Hole6;
            Hole7 = scorecard.Hole7;
            Hole8 = scorecard.Hole8;
            Hole9 = scorecard.Hole9;
            Hole10 = scorecard.Hole10;
            Hole11 = scorecard.Hole11;
            Hole12 = scorecard.Hole12;
            Hole13 = scorecard.Hole13;
            Hole14 = scorecard.Hole14;
            Hole15 = scorecard.Hole15;
            Hole16 = scorecard.Hole16;
            Hole17 = scorecard.Hole17;
            Hole18 = scorecard.Hole18;
            Out = scorecard.Hole1 + scorecard.Hole2 + scorecard.Hole3 + scorecard.Hole4 + 
                scorecard.Hole5 + scorecard.Hole6 + scorecard.Hole7 + scorecard.Hole8 + scorecard.Hole9;
            In = scorecard.Hole10 + scorecard.Hole11 + scorecard.Hole12 + scorecard.Hole13 +
                scorecard.Hole14 + scorecard.Hole15 + scorecard.Hole16 + scorecard.Hole17 + scorecard.Hole18;
            Total = Out + In;
            return this;
        }

    } 
}
