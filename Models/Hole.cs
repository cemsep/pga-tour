using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace pga_tour.Models
{
    public class Hole
    {
      [Key]
      public int Id { get; set; }
      public int HoleNr { get; set; }
      public int Par { get; set; }
      public int Length { get; set; }
      public float AverageScore { get; set; }

      [ForeignKey("Course")]
      public int CourseId { get; set; }
      public Course Course { get; set; }

      public Hole() { }

      public Hole(int holeNr, int par, int lenght, float averageScore, Course course) {
        HoleNr = holeNr;
        Par = par;
        Length = lenght;
        AverageScore = averageScore;
        Course = course;
      }
    }
}
