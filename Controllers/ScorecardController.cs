﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using pga_tour.Data;
using pga_tour.Models;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace pga_tour.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ScorecardController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public ScorecardController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: api/Scorecard
        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Scorecard>>> GetScorecards()
        {
            return await _context.Scorecards.ToListAsync();
        }

        // GET: api/Scorecard/5
        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<ActionResult<Scorecard>> GetScorecard(int id)
        {
            var scorecard = await _context.Scorecards.FindAsync(id);

            if (scorecard == null)
            {
                return NotFound();
            }

            return scorecard;
        }

        // PUT: api/Scorecard/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<ActionResult<Scorecard>> PutScorecard(int id, Scorecard scorecard)
        {
            Scorecard originalScorecard = await _context.Scorecards.FindAsync(id);

            if (id != scorecard.Id)
            {
                return BadRequest();
            }

            Scorecard updatedScorecard = originalScorecard.EditScorecard(scorecard);

            _context.Entry(updatedScorecard).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ScorecardExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return updatedScorecard;
        }

        // POST: api/Scorecard
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Scorecard>> PostScorecard(Scorecard scorecard)
        {
            _context.Scorecards.Add(scorecard);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch
            {
                if (ScorecardExists(scorecard.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }

            }

            return CreatedAtAction("GetScorecard", new { id = scorecard.Id }, scorecard);
        }

        // DELETE: api/Scorecard/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Scorecard>> DeleteScorecard(int id)
        {
            var scorecard = await _context.Scorecards.FindAsync(id);
            if (scorecard == null)
            {
                return NotFound();
            }

            _context.Scorecards.Remove(scorecard);
            await _context.SaveChangesAsync();

            return scorecard;
        }

        private bool ScorecardExists(int id)
        {
            return _context.Scorecards.Any(e => e.Id == id);
        }
    }
}
